<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Isaias</title>
</head>
<body>



<?php

// 1-
$nombres = array('Isaias', 'Jerobe' , 'Luis', 'Samu', 'Julia');
$nombres_s = array('Alumno1'=>'Isaias', 'Alumno2'=>'Jerobe', 'Alumno3'=>'Luis', 'Alumno4'=>'Samu', 'Alumno5'=>'Julia');

// 2- 
$cuenta = count($nombres);
echo "2- En el array hay : " . $cuenta . " nombres ";

?>
<br>
<?php

// 3- 
echo "3- " . implode(" ", $nombres);

?>
<br>
<?php

// 4-
asort($nombres);
echo "4- ";
foreach ($nombres as $alf){

echo "\n";
echo $alf;

};
echo "\n";
?>
<br>
<?php

// 5.1-
echo "5.1- ";
sort($nombres_s);
print_r($nombres_s)
?>
<br>
<?php

// 5.2-

$nombres_s = array('Alumno1'=>'Isaias', 'Alumno2'=>'Jerobe', 'Alumno3'=>'Luis', 'Alumno4'=>'Samu', 'Alumno5'=>'Julia');
echo "5.2- ";
ksort($nombres_s);
print_r($nombres_s)
?>
<br>
<?php
// 6-
echo "6- ";
print_r(array_reverse($nombres)); 
?>
<br>
<?php
// 7-
echo "7- ";
print_r(array_search("Isaias", $nombres));
?>
<br>
<?php
//8-
$alumnos = array(
'Alumno1' => array('id'=>100, 'nombre'=>'Isaias','edad' => 20),
'Alumno2' => array('id'=>101, 'nombre'=>'Jerobe','edad' => 20),
'Alumno3' => array('id'=>102, 'nombre'=>'Luis','edad' => 19),
'Alumno4' => array('id'=>103, 'nombre'=>'Julia','edad' => 22),
'Alumno5' => array('id'=>104, 'nombre'=>'Samu','edad' => 18),
);
?>
<!-- 9- -->
<?php
echo "9- ";
?>
<table>
<tr>
    <th>Id</th>
    <th>Nombre</th>
    <th>Edad</th>
</tr>

<?php for($i = 1; $i <= count($alumnos); $i++){ ?>

    <tr>
        <td><?php echo($alumnos["Alumno".$i]["id"])?></td>
        <td><?php echo($alumnos["Alumno".$i]["nombre"])?></td>
        <td><?php echo($alumnos["Alumno".$i]["edad"])?></td>
    </tr>

<?php }; ?>

</table>
<br>
<?php
// 10- 
echo "10- ";
print_r(array_column($alumnos , 'nombre'));
?>
<br>
<?php
// 11- 
$numeros = array(1,2,3,4,5,6,7,9,10);
echo "11- ";
print_r(array_sum($numeros));



?>




</body>
</html>