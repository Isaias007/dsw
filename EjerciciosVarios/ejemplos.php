<?php  //declare(strict_types=1); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplos</title>
</head>
<body>

<?php 

// Ejercicio 1 pruebas

function sum( $num1 , $num2 ): ?string{
    
    if ($num1 + $num2 > 5){
        return "El numero es mayor que 5";
    }else{
        return null;
    }
    
}

var_dump(sum(1,3));

?>
<hr>
<?php

function insert ($nombre_tabla, $datos) {

    $datos_nombre = implode(",", $datos);
    $datos_value = implode(":,", $datos);

echo "insert into $nombre_tabla ( $datos_nombre ) values ($datos_value:,) ";

}
$nombre_tabla = "trabajadores";
$campos = array("id","nombre", "apellido", "sueldo");


insert($nombre_tabla,$campos);

?>


    
</body>
</html>