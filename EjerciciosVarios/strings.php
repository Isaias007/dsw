<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <style>  
     </style>
     <title>Isaias</title>

</head>

<body>
<?php

    //Ejercicio 1

    //1- echo '¡Hola ' . htmlspecialchars($_GET["nombre"]) . '!';

    /*
    2-

    $nombre_conectado = $_GET["nombre"] ?? 'nadie';
    echo "¡Hola " . $nombre_conectado;  
    
    */

    /*
    3-

    $nombre_conectado = trim($_GET["nombre"], "/" )?? 'nadie';
    echo "¡Hola " . $nombre_conectado;  

    */

    //Ejercicio 2
 
    /*
    $longitud = strlen($_GET["nombre"]);
    echo "La longitud del parametro nombre es ". $longitud;
    */

    //Ejercicio 3 

    /*
    $nombre_minuscula = strtoupper($_GET["nombre"]);
    $nombre_mayuscula = strtolower($_GET["nombre"]);

    echo "El parametro nombre en minuscula es : " . $nombre_minuscula;
    echo "El parametro nombre en mayuscula es : " . $nombre_mayuscula;
    */

    //Ejercicio 4

    /*
    $posicion_pref = strpos($_GET["nombre"], $_GET["prefijo"]);
    echo "La posicion del prefijo empieza en el caracter " . $posicion_pref;
    */

    //Ejercicio 5


    /*
    $veces = substr_count($_GET["nombre"], strtolower("a")) + substr_count($_GET["nombre"], strtoupper("a"));
    echo "La letra a indistintamente de si es may o min sale : " . $veces . " veces";
    */

    //Ejercicio 6 
    
    // $posicion = strpos($_GET["nombre"], "a");
    /*
    if ($posicion){
        echo "La posicion es :" . $posicion;
    }else{
        echo "La posicion no existe";
    }
    */
 
    /*

    $msg = $posicion == true ? "La posicion es :". $posicion : "La posicion no existe";
    echo $msg;

    */


    // Ejercicio 7
    /*
    $nombre_cambiado = substr_replace($_GET["nombre"], "isa", 3);
    echo "El nombre remplazado seria " . $nombre_cambiado;
    */

    //Ejercicio 8 
    /*
    $url = 'http://username:password@hostname:9090/path?arg=value';
    print_r(parse_url($url));
    */




?>  



</body>



</html>


