<?php


// Array de errores y string de exito
$errores = [];
$exito = " ";

// Validacion del formulario

if ($_SERVER['REQUEST_METHOD']==='POST'){

    if (isset($_POST["nombre"]) && isset($_POST["correo"]) && isset($_POST["direccion"])){

        // Creacion de los errores  
        if(empty(trim($_POST["nombre"]))){
            array_push($errores, "Nombre esta vacio");
        }

        if(filter_var($_POST["correo"], FILTER_VALIDATE_EMAIL)==FALSE){
            array_push( $errores, "EMAIL mal escrito" );
        }

        if(empty($_POST["correo"])){
            array_push($errores, "El correo esta vacio");
        }

        if(empty(trim($_POST["direccion"]))){
            array_push($errores, "La direccion esta vacia");
        }

        
        // Evitamos la insercion de codigo
        
        $nombre =htmlentities($_POST["nombre"]);
        $apellidos = htmlentities($_POST["apellidos"]);
        $titulo = htmlentities($_POST["titulo"]);
        $telefono = htmlentities($_POST["telefono"]);
        $direccion = htmlentities($_POST["direccion"]);
        $ocupacion = htmlentities($_POST["ocupacion"]);
        $correo = htmlentities($_POST["correo"]);

        // Contenido del archivo que se va a crear

        $contenido = 

        "Nombre : ".$nombre." ".$apellidos ."\n".
        "Email : ". $correo."\n".
        "Telefono : ".$telefono."\n".
        "Direccion : ".$direccion."\n".
        "titulo : ".$titulo."\n".
        "ocupacion :".$ocupacion."\n";



        // Creacion del archivo en la carpeta tmp
        $archivo = fopen("tmp/$correo.txt", "w+");
        fwrite($archivo,$contenido);
        fclose($archivo);
        
        if (file_exists("tmp/$correo.txt")){
            $exito = "Se ha enviado todo con exito";
        }
        // Vista
        require "views/index.view.php";

        
    }

    

}else{
    require "views/index.view.php";
}





?>



